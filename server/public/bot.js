var botui = new BotUI('hello-world');
var name = "";
var expect_name = false;

function askForInput() {
    return botui.action.text({
        action: {
            placeholder: ''
        }
    })
}

function handleResponse(response) {
    console.log('Intent: ' + response.intent);
    if (response.intent === 'greetings.hello'){
        botui.message.add({
            content: response.answer
        }).then(function () {
            expect_name = true;
            console.log('conversation starting');
        });
    }else if (response.intent === 'greetings.bye') {
        botui.message.add({
            content: response.answer
        }).then(function () {
            console.log('conversation is over');
        });
    } else if (response.intent === 'inquiries.who') {
        return botui.message.add({
            content: response.answer
        }).then(function () {
            return botui.message.add({
                type: 'embed',
                content: 'https://cdn2.iconfinder.com/data/icons/walle/256/my_computer.png'
            });
        });
    } else if (response.intent === 'inquiries.movie') {
        if (response.entities.length <= 0) {
            return botui.message.add({
                content: 'You wanted to know about which movie?'
            });
        } else if (response.entities[0].entity === 'movie') {
            var movie_title = response.entities[0].option;
            getMovie(movie_title);
            return botui.message.add({
                content: 'Okay infos about ' + movie_title + ' coming up, what do you wanna know?'
            });
        }
    } else if (response.intent === 'movie.actors') {
        if (!movie_infos) {
            return botui.message.add({
                content: 'You wanted to know about which movie?'
            });
        } else {
            return botui.message.add({
                content: movie_infos['Actors']
            });
        }
        console.log("user asked for actors")
    } else if (response.intent === 'movie.rating') {
        if (!movie_infos) {
            return botui.message.add({
                content: 'You wanted to know about which movie?'
            });
        } else {
            return botui.message.add({
                content: movie_infos['Ratings']
            });
        }       
        console.log("user asked for rating")
    } else if (response.intent === 'movie.plot') {
        if (!movie_infos) {
            return botui.message.add({
                content: 'You wanted to know about which movie?'
            });
        } else {
            return botui.message.add({
                content: movie_infos['Plot']
            });
        }        
        console.log("user asked for plot")
    } else if (response.intent === 'movie.genre') {
        if (!movie_infos) {
            return botui.message.add({
                content: 'You wanted to know about which movie?'
            });
        } else {
            return botui.message.add({
                content: movie_infos['Genre']
            });
        }
        console.log("user asked for genre")
    } else if (response.intent === 'inquiries.love') {
        return botui.message.add({
            content: response.answer
        }).then(function () {
            return botui.message.add({
                delay: 1000,
                loading: true,
                type: 'embed',
                content: 'https://images.ecosia.org/6W1W_3MYbY8asUJN-ahlFQxb50w=/0x390/smart/http%3A%2F%2Fopenwalls.com%2Fimage%2F19038%2Fwall_e_and_eve_1_1920x1378.jpg'
            });
        });
    } else if (response.intent === 'inquiries.likes') {
        return botui.message.add({
            content: response.answer
        }).then(function () {
            return botui.action.button({
                action: [
                    {
                        text: 'Yes',
                        value: 'yes'
                    },
                    {
                        text: 'No',
                        value: 'no'
                    }
                ]
            }).then(function (result) {
                if (result.value === 'yes') {
                    return botui.message.add({
                        content: 'Hal has been my friendly companion for quite a while and he is the best cockroach on the earth.'
                    }).then(function () {
                        return botui.message.add({
                            delay: 1000,
                            loading: true,
                            type: 'embed',
                            content: 'https://images.ecosia.org/eZ8-b34C0O2OAStfXkoujB9ek2Q=/0x390/smart/http%3A%2F%2Fn2citrus.com%2Fimages%2Fwall_e%2FHAL.jpg'
                        });
                    }).then(askAndAnalyze);
                } else {
                    return botui.message.add({
                        delay: 2000,
                        loading: true,
                        content: 'Okay..'
                    }).then(askAndAnalyze);
                }
            });
        });
    } else if (response.intent && response.intent !== 'None') {
        return botui.message.add({
            content: response.answer
        })
    } else {
        if (expect_name){
            console.log(response);
            name = response.utterance;
            expect_name = false;
            return botui.message.add({
                content: 'Hello ' + name + '! Nice to meet you! \nAs I really have a lot of time whilst cleaning up here. I have the deepest knowledge of movies you can imagine. You can ask me about plots, actors and actresses or ratings of movies!'
            })
        }
        if (response.sentiment) {
            if (response.sentiment.vote === 'positive') {
                return botui.message.add({
                    content: 'That is really nice thank you'
                });
            } else if (response.sentiment.vote === 'negative') {
                return botui.message.add({
                    content: 'Hey, no need to be rude!'
                });
            }
        }
        return botui.message.add({
            content: 'I am sorry, I did not really understand you. You can ask me something like my age, my hobbies, etc or you can ask me to query my brain for some movie infos.'
        });
    }
}

var movie_infos = null;
function getMovie(movie) {
    return $.post('/get-movie', {
        movie: movie
    }, function success(response) {
        movie_infos = JSON.parse(response);
    });
}

function analzyeInput(userInput) {
    return $.post('/analyze', {
        text: userInput.value
    }, function success(response) {
        return handleResponse(response);
    });
}

function askAndAnalyze() {
    askForInput()
        .then(analzyeInput)
        .then(askAndAnalyze);
}

askAndAnalyze();
