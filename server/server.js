var express = require('express');
var bodyParser = require("body-parser");
var http = require('http');
var https = require('https');
var app = express();
var server = http.createServer(app);
var port = 1337;
const { NlpManager, ConversationContext, NerManager } = require('node-nlp');

const manager = new NlpManager({ languages: ['en'] });
const ner_manager = new NerManager({ threshold: 0.8 });
const context = new ConversationContext();

// ------- GREETINGS -------
manager.addDocument('en', 'goodbye for now', 'greetings.bye');
manager.addDocument('en', 'bye bye take care', 'greetings.bye');
manager.addDocument('en', 'okay see you later', 'greetings.bye');
manager.addDocument('en', 'bye for now', 'greetings.bye');
manager.addDocument('en', 'i must go', 'greetings.bye');

manager.addDocument('en', 'hello', 'greetings.hello');
manager.addDocument('en', 'hi', 'greetings.hello');
manager.addDocument('en', 'howdy', 'greetings.hello');

manager.addAnswer('en', 'greetings.bye', 'Till next time');
manager.addAnswer('en', 'greetings.bye', 'See you soon!');
manager.addAnswer('en', 'greetings.hello', 'Hi! I\m so glad to see you. I\'ve been cleaning up around here all alone for a quite a while. I would love some company, what is your name?');
manager.addAnswer('en', 'greetings.hello', 'Hello there, I\'ve been alone for quite some time, great to see you. What is your name?');
manager.addAnswer('en', 'greetings.hello', 'Greetings friend !(check) What is your name?');

// ------- INQUIRIES -------

manager.addDocument('en', 'My name is %firstname%', 'infos.name');
manager.addDocument('en', 'My name is %firstname% and I am %age% years old', 'infos.name');
manager.addDocument('en', 'I am %firstname%', 'infos.name');
manager.addDocument('en', 'I am %firstname% and %age% years.', 'infos.name');

manager.addAnswer('en', 'infos.name', 'What a nice name. Do you want to tell me a bit more about yourself?');

manager.addDocument('en', 'How are you doing?', 'inquiries.being');
manager.addDocument('en', 'How are you?', 'inquiries.being');

manager.addAnswer('en', 'inquiries.being', 'I am doing great thanks!');

manager.addDocument('en', 'What is your age?', 'inquiries.age');
manager.addDocument('en', 'How old are you?', 'inquiries.age');
manager.addDocument('en', 'How long have you been here?', 'inquiries.age');

manager.addAnswer('en', 'inquiries.age', 'I think it was about 700 years ago that I started cleaning up earth - that\'s how long I\'ve been around.');

manager.addDocument('en', 'Who are you?', 'inquiries.who');
manager.addDocument('en', 'What is your name?', 'inquiries.who');

manager.addAnswer('en', 'inquiries.who', 'I\'m sure you know me! Let me show you a picture!');

manager.addDocument('en', 'What do you like to do?', 'inquiries.likes');
manager.addDocument('en', 'What are your hobbies?', 'inquiries.likes');
manager.addDocument('en', 'What are you doing the whole day?', 'inquiries.likes');

manager.addAnswer('en', 'inquiries.likes', 'During my cleaning mission I find many peculiar things that I love to collect and of course I love spending time with my best friend Hal. Would you like to know more about him?');
manager.addAnswer('en', 'inquiries.likes', 'I love collecting, just recently I found this awesome cube with different colors on it and some kind of foliage that made funny sounds when you popped it. And my best friend Hal is always with me - do you want to know more about him?');

manager.addDocument('en', 'Are you in love?', 'inquiries.love');
manager.addDocument('en', 'And is there a special robot for you?', 'inquiries.love');
manager.addDocument('en', 'Is there someone you more than like?', 'inquiries.love');

manager.addAnswer('en', 'inquiries.love', 'A picture says more than thousand words');
manager.addAnswer('en', 'inquiries.love', 'Thankfully there is someone special for me');

// ------- INFOS -------

manager.addDocument('en', 'And what can I do now?', 'inquiries.what');
manager.addDocument('en', 'What are you doing now?', 'inquiries.what');
manager.addDocument('en', 'So, what now?', 'inquiries.what');

manager.addAnswer('en', 'inquiries.what', 'As I really have a lot of time whilst cleaning up here. I have the deepest knowledge of movies you can imagine. You can ask me about plots, actors and actresses or ratings of movies!');

// ------- MOVIES -------

// Actors
manager.addDocument('en', 'who is starring', 'movie.actors');
manager.addDocument('en', 'who is involved', 'movie.actors');
manager.addDocument('en', 'which people are in', 'movie.actors');
manager.addDocument('en', 'who plays in', 'movie.actors');
manager.addDocument('en', 'who is the actor in', 'movie.actors');
manager.addDocument('en', 'who is acting in', 'movie.actors');
manager.addDocument('en', 'who are the actors', 'movie.actors');


// Rating
manager.addDocument('en', 'whats the rating of', 'movie.rating');
manager.addDocument('en', 'is it a good movie', 'movie.rating');
manager.addDocument('en', 'should i watch', 'movie.rating');
manager.addDocument('en', 'how many points', 'movie.rating');
manager.addDocument('en', "how much is the movie rated", 'movie.rating');

//Plot
manager.addDocument('en', 'what is it about', 'movie.plot');
manager.addDocument('en', "what's the plot", 'movie.plot');
manager.addDocument('en', 'tell me the story', 'movie.plot');

// Genre
manager.addDocument('en', 'What category is it', 'movie.genre');
manager.addDocument('en', 'which genre', 'movie.genre');
manager.addDocument('en', 'what kind of', 'movie.genre');


// ------- NER -------

ner_manager.addNamedEntityText('movie', 'guardians+of+the+galaxy', ['en'], ['guardians of the galaxy']);
ner_manager.addNamedEntityText('movie', 'titanic', ['en'], ['titanic']);
ner_manager.addNamedEntityText('movie', 'forrest+gump', ['en'], ['forrest gump']);
ner_manager.addNamedEntityText('movie', 'wall-e', ['en'], ['wall-e', 'walle', 'wall e']);

// ------- MOVIE INFOS -------

manager.addDocument('en', 'Can you tell me something about the movie %movie%?', 'inquiries.movie');
manager.addDocument('en', 'Can you tell me something about the film %movie%?', 'inquiries.movie');
manager.addDocument('en', 'I want to know about the %movie%', 'inquiries.movie');
manager.addDocument('en', 'Tell me about the %movie%', 'inquiries.movie');

// -------   MODEL   -------
(async () => {
    await manager.train();
    manager.save();
    console.log('manager trained');
})();

// start the web server.
server.listen(port, function () {
    console.log('Webserver läuft und hört auf Port %d', port);
});

// express path to which content the web server should be able to serve
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/analyze', function (req, res) {
    (async () => {
        console.log('analysing request: ', req.body.text);

        const result = await manager.process('en', req.body.text, context);
        if (result.intent !== 'None' && result.classification[0].value < 0.7) {
            result.intent = 'None';
        } else if (result.intent === 'inquiries.movie') {
            const entities = await ner_manager.findEntities(req.body.text, 'en');
            result.entities = entities;
        }
        res.send(result);
    })();
});

const movie_options = {
    hostname: 'www.omdbapi.com',
    path: '/?i=tt3896198&apikey=181ef8e4&t=',
    method: 'GET'
}

app.post('/get-movie', function (req, res) {
    (async () => {
        console.log('query to OMDB: ', req.body.movie);
        var request = https.request(movie_options, function (response) {
            if (response.statusCode === 200) {
                var movie_data = '';
                response.on('data', (chunk) => {
                    console.log(`BODY: ${chunk}`);
                    movie_data = movie_data + chunk;
                });
                response.on('end', () => {
                    res.send(movie_data);
                })
            }
        });
        request.path = request.path + req.body.movie;
        request.end();
    })();
});

const options = {
    hostname: 'westcentralus.api.cognitive.microsoft.com',
    path: '/text/analytics/v2.0/entities',
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Ocp-Apim-Subscription-Key': '686fafabee6d4defb734da7b2e6b8660'
    }
};

app.post('/extract-entities', function (req, res) {
    (async () => {
        console.log('calling azure entity extraction: ', req.body.text);
        var postData = '{ "documents": [ { "language": "en", "id": "1", "text": "' + req.body.text + '" }] }'
        var request = https.request(options, function (response) {
            if (response.statusCode === 200) {
                response.on('data', (chunk) => {
                    console.log(`BODY: ${chunk}`);
                    res.send(response.data);
                });
            }
        });
        request.write(postData);
        request.end();
    })();
});

const sentiment_options = {
    hostname: 'westcentralus.api.cognitive.microsoft.com',
    path: '/text/analytics/v2.0/sentiment',
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Ocp-Apim-Subscription-Key': '686fafabee6d4defb734da7b2e6b8660'
    }
};

app.post('/sentiment', function (req, res) {
    (async () => {
        console.log('calling azure entity extraction: ', req.body.text);
        var postData = '{ "documents": [ { "language": "en", "id": "1", "text": "' + req.body.text + '" }] }'
        var https = require('https');
        var request = https.request(sentiment_options, function (response) {
            res.send(response);
        });
        request.write(postData);
        request.end();
    })();
});