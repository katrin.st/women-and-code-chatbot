# Womenandcode hackathon Chatbots Demo

Small demo for a JavaScript chatbot using [Bot-UI](https://docs.botui.org/index.html), [NLP JS](https://github.com/axa-group/nlp.js) and Cognitive Services / imdb API. 

## How To 

1. Make sure you have NodeJs installed
2. Navigate to /server
2. execute "npm install "
3. start your server with "node server.js"
4. in your webbrowser open "localhost:1337"